#!/usr/bin/env bash

BASE_DIR=$(dirname "${0}")
CFG_FILE="linky.cfg"

# check configuration file
if [ ! -f "${BASE_DIR}"/"${CFG_FILE}" ]
then
    echo "Config file is missing ["${BASE_DIR}"/"${CFG_FILE}"]"
    exit 1
fi

source "${BASE_DIR}"/"${CFG_FILE}"

source ${LINKY_VENV}/bin/activate

DB_DIR=${LINKY_DATADIR}
DB_FILE="linky.sqlite"
LOG_DIR=${LINKY_LOGDIR}
LOG_FILE="linky.log"

PY_SCRIPT=linky_grabber.py

exec 3<<<"${LINKY_USERNAME}:${LINKY_PASSWORD}"
unset LINKY_PASSWORD
${PY_SCRIPT} -v --credentials /dev/fd/3 --sqlite3-db ${DB_DIR}/${DB_FILE} --meter-name ${LINKY_NAME} --data-dir "${DB_DIR}/$(date +%Y%m%d)" --log ${LOG_DIR}/${LOG_FILE}
exit $?


