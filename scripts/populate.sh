#!/usr/bin/env bash

BASE_DIR=$(dirname "${0}")
CFG_FILE="linky.cfg"

# check configuration file
if [ ! -f "${BASE_DIR}"/"${CFG_FILE}" ]
then
    echo "Config file is missing ["${BASE_DIR}"/"${CFG_FILE}"]"
    exit 1
fi

source "${BASE_DIR}"/"${CFG_FILE}"

source ${LINKY_VENV}/bin/activate

linky_grabber.py --create --meter-name ${LINKY_NAME}

DATADIR=${LINKY_DATADIR}
#DBG=-d

linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period days --from-files $DATADIR/*/*_days_values.txt
linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period hours --from-files $DATADIR/*/*_hours_values.txt
linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period months --from-files $DATADIR/*/*_month*_values.txt
linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period years --from-files $DATADIR/*/*_years_values.txt

linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period hours --from-files $DATADIR/linky_hours.txt
linky_grabber.py -v $DBG --meter-name ${LINKY_NAME} --files-period days --from-files $DATADIR/linky_days.txt


